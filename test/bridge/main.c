#include "mock_zmsg.h"
#include <setjmp.h>

#include <cjson/cJSON.h>
#include <cmocka.h>

#include "bridge.h"

int test_bridge_alert_callback(zmtp_bridge* bridge, zmtp_bridge_alert* alert);
static int test_bridge_alert_callback_count = 0;

zmtp_bridge_settings g_test_settings = { //
    .on_alert = test_bridge_alert_callback
};

int
test_bridge_alert_callback(zmtp_bridge* bridge, zmtp_bridge_alert* alert)
{
    ((void)bridge);
    assert_string_equal(alert->who, "test-serial-id");
    assert_string_equal(alert->name, "pOn");
    assert_string_equal(alert->where, "Altronix Site ID");
    assert_string_equal(alert->mesg, "Power Supply Turn On");
    assert_int_equal(alert->when, 12345678);
    test_bridge_alert_callback_count++;
    return 0;
}

zmsg_t*
test_bridge_make_alert()
{
    zmsg_t* msg = zmsg_new();
    zframe_t *rid, *typ, *sid, *dat;
    const char* alert = "{"
                        "\"meth\":\"POST\","
                        "\"path\":\"home/exe/alert\","
                        "\"post\":{"
                        "\"who\":\"test-serial-id\","
                        "\"what\":\"Power Supply 1\","
                        "\"siteId\":\"Altronix Site ID\","
                        "\"when\":12345678,"
                        "\"name\":\"pOn\","
                        "\"mesg\":\"Power Supply Turn On\""
                        "}"
                        "}";

    if (msg) {
        rid = zframe_new("sid", 3);
        sid = zframe_new("rid", 3);
        typ = zframe_new("typ", 3);
        dat = zframe_new(alert, strlen(alert) + 1);
        if (!(rid && sid && typ && dat)) {
            zmsg_destroy(&msg);
            if (rid) zframe_destroy(&rid);
            if (sid) zframe_destroy(&sid);
            if (typ) zframe_destroy(&typ);
            if (dat) zframe_destroy(&dat);
        } else {
            zmsg_append(msg, &rid);
            zmsg_append(msg, &typ);
            zmsg_append(msg, &sid);
            zmsg_append(msg, &dat);
        }
    }
    return msg;
}

static int
test_bridge_start(void** context_p)
{
    *context_p = zmtp_bridge_create(&g_test_settings, context_p);
    return 0;
}

static int
test_bridge_end(void** context_p)
{
    zmtp_bridge* bridge = *context_p;
    zmtp_bridge_destroy(&bridge);
    return 0;
}

static void
test_bridge_mock_incoming(void** context)
{
    ((void)context);
    zmsg_t *a, *b, *c;

    // Populate linked listed with incoming messages and read them all. Then do
    // it again to be sure the list can be repeatedly empties and full again.
    a = test_bridge_make_alert();
    b = test_bridge_make_alert();
    c = test_bridge_make_alert();
    czmq_spy_push_incoming_mesg(&a);
    czmq_spy_push_incoming_mesg(&b);
    czmq_spy_push_incoming_mesg(&c);
    assert_null(a);
    assert_null(b);
    assert_null(c);
    a = zmsg_recv(NULL);
    b = zmsg_recv(NULL);
    c = zmsg_recv(NULL);
    assert_non_null(a);
    assert_non_null(b);
    assert_non_null(c);
    zmsg_destroy(&a);
    zmsg_destroy(&b);
    zmsg_destroy(&c);
    a = test_bridge_make_alert();
    assert_non_null(a);
    czmq_spy_push_incoming_mesg(&a);
    a = zmsg_recv(NULL);
    assert_non_null(a);
    zmsg_destroy(&a);
}

static void
test_bridge_create(void** context_p)
{
    zmtp_bridge* bridge = zmtp_bridge_create(&g_test_settings, context_p);
    assert_non_null(bridge);
    assert_ptr_equal(bridge->settings, &g_test_settings);
    assert_ptr_equal(bridge->context, context_p);
    zmtp_bridge_destroy(&bridge);
    assert_null(bridge);
}

static void
test_bridge_listen(void** context_p)
{
    ((void)context_p);
    int err;
    zmtp_bridge* bridge = zmtp_bridge_create(&g_test_settings, context_p);
    err = zmtp_bridge_listen(bridge, "tcp://127.0.0.1:9090");
    assert_int_equal(err, 0);
    zmtp_bridge_destroy(&bridge);
}

static void
test_bridge_recv(void** context_p)
{
    zmtp_bridge* bridge = *context_p;
    int err;
    zmsg_t* alert;

    // Spoof an incoming message and read it, verify our callbacks are fired.
    test_bridge_alert_callback_count = 0;
    alert = test_bridge_make_alert();
    czmq_spy_push_incoming_mesg(&alert);
    err = zmtp_bridge_recv(bridge);
    assert_int_equal(err, 0);
    assert_int_equal(test_bridge_alert_callback_count, 1);
    test_bridge_alert_callback_count = 0;
}

int
main(int argc, char* argv[])
{
    ((void)argc);
    ((void)argv);
    int err;
    const struct CMUnitTest tests[] = //
        {                             //
          cmocka_unit_test(test_bridge_create),
          cmocka_unit_test(test_bridge_listen),
          cmocka_unit_test(test_bridge_mock_incoming),
          cmocka_unit_test(test_bridge_recv)
        };

    err = cmocka_run_group_tests(tests, test_bridge_start, test_bridge_end);
    return err;
}
