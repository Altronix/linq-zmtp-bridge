## Installation ##

```
git clone https://bitbucket.org/altronix/linq-zmtp-bridge
cd linq-zmtp-bridge
cmake -DCMAKE_INSTALL_PREFIX=../target #cross compiles like local install
make
make install
```

## Build tests ##

- Run quick, verbose, memory, or coverage (respectively)

```
make test
ctest -V
ctest -T memcheck
```
