find_package(Threads)

add_library(zmtp-bridge bridge.c bridge.h)

target_link_libraries(zmtp-bridge cjson czmq zmq ${CMAKE_THREAD_LIBS_INIT} rt uuid)
target_include_directories(zmtp-bridge PUBLIC .)
