#ifndef BRIDGE_H_
#define BRIDGE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "czmq.h"

#define zmtp_bridge_malloc_fn malloc
#define zmtp_bridge_free_fn free

typedef struct zmtp_bridge zmtp_bridge;
typedef struct zmtp_mqtt_callback zmtp_mqtt_callback;
typedef struct zmtp_bridge_alert zmtp_bridge_alert;
typedef int (*zmtp_alert_fn)(zmtp_bridge* bridge, zmtp_bridge_alert* alert);

/**
 * @brief an alert
 */
typedef struct zmtp_bridge_alert
{
    const char* who;   /*!< Serial number of the LinQ product */
    const char* what;  /*!< Thing that generated alert */
    const char* where; /*!< Site ID of the location of the product */
    const char* mesg;  /*!< human readable meta */
    uint32_t when;     /*!< Unix time stamp */
    const char* name;  /*!< short string defining the alert */
    uint32_t id;       /*!< a local id of the product alert */
} zmtp_bridge_alert;

/**
 * @brief caller initalize settings
 */
typedef struct zmtp_bridge_settings
{
    zmtp_alert_fn on_alert;
} zmtp_bridge_settings;

/**
 * @brief Main API handle
 */
typedef struct zmtp_bridge
{
    void* context;                  /*!< callers callback context */
    zmtp_bridge_settings* settings; /*!< zmtp settigns */
    zsock_t* sock;                  /*!< socket */
} zmtp_bridge;

zmtp_bridge* zmtp_bridge_create(zmtp_bridge_settings* settings, void* context);

void zmtp_bridge_destroy(zmtp_bridge** bridge_p);

int zmtp_bridge_listen(zmtp_bridge* bridge, const char* endpoint);

int zmtp_bridge_poll(zmtp_bridge* bridge);

int zmtp_bridge_recv(zmtp_bridge* bridge);

#ifdef __cplusplus
}
#endif
#endif
