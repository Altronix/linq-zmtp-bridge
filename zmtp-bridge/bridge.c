#include "bridge.h"
#include <cjson/cJSON.h>

int zmtp_bridge_process_message(zmtp_bridge* bridge, zmsg_t* message);
int zmtp_bridge_process_json(zmtp_bridge* bridge, const cJSON* json);
int zmtp_bridge_process_alert(zmtp_bridge* bridge, const cJSON* alert);

zmtp_bridge*
zmtp_bridge_create(zmtp_bridge_settings* settings, void* context)
{
    zmtp_bridge* bridge = NULL;
    bridge = zmtp_bridge_malloc_fn(sizeof(zmtp_bridge));
    if (bridge) {
        bridge->sock = NULL;
        bridge->settings = settings;
        bridge->context = context;
    }
    return bridge;
}

void
zmtp_bridge_destroy(zmtp_bridge** bridge_p)
{
    zmtp_bridge* bridge = *bridge_p;
    *bridge_p = NULL;
    if (bridge->sock) zsock_destroy(&bridge->sock);
    zmtp_bridge_free_fn(bridge);
}

int
zmtp_bridge_listen(zmtp_bridge* bridge, const char* endpoint)
{
    if (bridge->sock) return -1;
    bridge->sock = zsock_new_router(endpoint);
    return bridge->sock ? 0 : -1;
}

int
zmtp_bridge_poll(zmtp_bridge* bridge)
{
    int err;
    zmq_pollitem_t item = { zsock_resolve(bridge->sock), 0, ZMQ_POLLIN, 0 };
    err = zmq_poll(&item, 1, 1000);
    if (err < 0) return err;
    if (item.revents && ZMQ_POLLIN) {
        err = zmtp_bridge_recv(bridge);
    }
    return err;
}

int
zmtp_bridge_recv(zmtp_bridge* bridge)
{
    int err;
    zmsg_t* msg = zmsg_recv(bridge->sock);
    if ((msg) && (!(zmsg_size(msg) < 3))) {
        err = zmtp_bridge_process_message(bridge, msg);
        zmsg_destroy(&msg);
    } else {
        err = -1;
    }
    return err;
}

int
zmtp_bridge_process_message(zmtp_bridge* bridge, zmsg_t* message)
{

    cJSON* json;
    const char *ptr, *end;
    zframe_t* frame;
    ((void)bridge);

    // pop off router id / typ / serial / json data
    for (int i = 0; i < 3; i++) {
        frame = zmsg_pop(message);
        if (frame) zframe_destroy(&frame);
    }

    // Parse the json data and fire callbacks
    frame = zmsg_pop(message);
    if (frame) {
        ptr = (const char*)zframe_data(frame);
        end = ptr + (zframe_size(frame) - 1);
        json = cJSON_ParseWithOpts(ptr, &end, false);
        if (json) {
            zmtp_bridge_process_json(bridge, json);
            cJSON_Delete(json);
        }
        zframe_destroy(&frame);
        return 0;
    }
    return -1;
}

int
zmtp_bridge_process_json(zmtp_bridge* bridge, const cJSON* json)
{
    cJSON *jpath, *jmeth, *post;
    const char *path, *meth;
    int err = -1, pathlen;
    if (!((jpath = cJSON_GetObjectItem(json, "path")) &&
          (jmeth = cJSON_GetObjectItem(json, "meth")) &&
          (post = cJSON_GetObjectItem(json, "post")) &&
          (path = cJSON_GetStringValue(jpath)) &&
          (meth = cJSON_GetStringValue(jmeth)))) {
        return -1;
    }
    pathlen = strlen(path);
    if (((pathlen >= 14) && (!(memcmp(path, "home/exe/alert", 15)))) ||
        ((pathlen >= 24) &&
         (!(memcmp(path, "production/exe/heartbeat", 25))))) {
        err = zmtp_bridge_process_alert(bridge, post);
    }
    return err;
}

int
zmtp_bridge_process_alert(zmtp_bridge* bridge, const cJSON* alert)
{
    cJSON *jwho, *jwhat, *jwhen, *jwhere, *jmesg, *jname;
    zmtp_bridge_alert cb;
    int err;
    if (!((jwho = cJSON_GetObjectItem(alert, "who")) &&
          (jwhat = cJSON_GetObjectItem(alert, "what")) &&
          (jwhere = cJSON_GetObjectItem(alert, "siteId")) &&
          (jwhen = cJSON_GetObjectItem(alert, "when")) &&
          (jname = cJSON_GetObjectItem(alert, "name")) &&
          (jmesg = cJSON_GetObjectItem(alert, "mesg")) &&
          (cb.who = cJSON_GetStringValue(jwho)) &&
          (cb.what = cJSON_GetStringValue(jwhat)) &&
          (cb.where = cJSON_GetStringValue(jwhere)) &&
          (cb.mesg = cJSON_GetStringValue(jmesg)) &&
          (cb.when = jwhen->valueint) &&
          (cb.who = cJSON_GetStringValue(jwho)) &&
          (cb.name = cJSON_GetStringValue(jname)))) {
        return -1;
    }
    err = bridge->settings->on_alert(bridge, &cb);
    return err;
}
