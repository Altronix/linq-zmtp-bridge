#include "bridge.h"
#include <signal.h>

// Prototypes
int main(int argc, char* argv[]);
int app_alert_callback(zmtp_bridge* bridge, zmtp_bridge_alert* alert);

// Demo app context
typedef struct
{
    char* string;
} app_context;

// Callback when alerts are detected
int
app_alert_callback(zmtp_bridge* bridge, zmtp_bridge_alert* alert)
{
    app_context* app = bridge->context;
    ((void)app);
    printf(
        "\n\nWHO: %s\nWHAT: %s\nWHERE: %s\nMESG: %s\n\n",
        alert->who,
        alert->what,
        alert->where,
        alert->mesg);
    return 0;
}

// Initialize bridge
zmtp_bridge_settings bridge_settings = {.on_alert = app_alert_callback };

// main
int
main(int argc, char* argv[])
{
    ((void)argc);
    ((void)argv);
    int err;
    app_context app = {.string = "hello" };

    // Create bridge
    zmtp_bridge* bridge = zmtp_bridge_create(&bridge_settings, &app);
    if (!bridge) return -1; // memory

    // Listen for incoming connections
    err = zmtp_bridge_listen(bridge, "tcp://127.0.0.1:9000");
    if (err) {
        zmtp_bridge_destroy(&bridge);
        return err;
    }

    // Listen for alerts
    while (true) {
        err = zmtp_bridge_poll(bridge);
        if (err) break;
    }

    zmtp_bridge_destroy(&bridge);
    return 0;
}
